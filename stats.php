<?php
require 'inc/conf.php';
require 'inc/constants.php';
require 'inc/init.php';
require 'inc/functions.php';
require 'inc/classes.php';
require 'inc/dbmanager.php';

$dbManager = new DbManager();

//clean old data
$time = $_SERVER['REQUEST_TIME'];
$dbManager->CleanChatrooms($time);

$nbChatrooms = $dbManager->GetNbChatRooms();
$nbMessages = $dbManager->GetNbMessages();
$nbActiveUsers = $dbManager->GetNbActiveUsers();

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Stats - MyCryptoChatEx by salikovi.cz</title>
    
    <link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />       
    
    <meta name="viewport" content="width=device-width" />    
    
  <?php if(!defined('DEV_MODE')): ?>    
    <link href="styles/rel/myCryptoChat.min.css" rel="stylesheet" />
    <script src="scripts/rel/modernizr-2.6.2.min.js"></script>
  <?php else: ?>     
    <link href="styles/dev/myCryptoChat.css" rel="stylesheet" />
    <script src="scripts/dev/modernizr-2.6.2.js"></script>
  <?php endif; ?>     
    
</head>
<body>
    <header>
        <div class="content-wrapper">
            <div class="float-left">
                <p class="site-title"><a href="index.php">MyCryptoChatEx</a></p>
            </div>
            <div class="float-right">
                <section id="login">
                </section>
                <nav>
                    <ul id="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="stats.php">Stats</a></li>
                        <li><a href="about.php">About</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <div id="body">
        <section class="content-wrapper main-content clear-fix">

            <h2>Stats about MyCryptoChatEx</h2>

            <p>
                Number of chat rooms: <?php echo $nbChatrooms; ?><br />
                Number of messages: <?php echo $nbMessages; ?><br />
                Number of active users: <?php echo $nbActiveUsers; ?>
            </p>

        </section>
    </div>
    <footer>
        <div class="content-wrapper">
            <div class="float-left">
                <p>&copy; 2016 - MyCryptoChatEx <?php echo MYCRYPTOCHAT_VERSION; ?> by salikovi.cz</p>
            </div>
        </div>
    </footer>
  
  <?php if(!defined('DEV_MODE')): ?>    
    <script src="scripts/rel/jquery.min.js"></script>
  <?php else: ?>     
    <script src="scripts/dev/jquery.js"></script>
  <?php endif; ?>        
    
</body>
</html>
