Array.prototype.random = function(){return this[Math.floor(Math.random() * this.length)];};
    
function generatePassword_CZECH(random_words_count){  
  var str = '';  
  var words = '';    
  
  if (random_words_count > 0) { 
    for (var i = 0; i < random_words_count; i++) { 
      words = sentences.random().toString().split(" ");    
      str = str + words.random() + ' ';
    }

    str = str.replace(/\./g, '');
    str = str.replace(/,/g, '');
    str = str.toLowerCase().trim();    
  } else {
    words = sentences.random().toString();    
  }
  
  str = words.trim();
  
  return str;
}