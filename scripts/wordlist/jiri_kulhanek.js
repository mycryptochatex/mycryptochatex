var sentences =
[
  ["Mrtvák se mi zakousl do ramene."],
  ["Hlava hostinského Kozáka byla široko daleko."],
  ["Začátečníkům občas štěstí přeje."],
  ["Kdo se smrtí se nesmíří, života neužije."],
  ["Zbraň je polovinou bojovníka."],
  ["Bojovník, který myslí na prohru, je zpoloviny poražený."],
  ["Středně velcí pavouci jsou nejlepší."],
  ["Pilot je ten, kdo létá, ne kdo padá."],
  ["Hezký den na bitvu."],
  ["Hezký den na smrt."],
  ["Tady máte kartáček na zuby."],
  ["V rachotu rotačního kulometu zaniklo všechno."],
  ["Pořád jsem pan Hodný."],
  ["Bratr byl překvapen, že mu s granátem zmizelo i předloktí."],
  ["Jsou to docela působivé plameny."],
  ["A Zářící Spasitel mému úsměvu uvěřil."],
  ["Co tě nezabije to tě posílí."],
  ["Nakonec jsem v rakvi strávil celý měsíc."],
  ["Bohužel jsem zapomněl číslo konta, takže si radši vezmu všechno."]
];