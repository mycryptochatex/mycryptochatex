//http://refresh-sf.com

function sendMessage() {
    if ($.trim($("#userName").val()) == "") {
      //alert("Please choose a name"!");
      $.WarningDialog('Please choose a name"!', 'Chat user name',
        function() { //OK
          $("#userName").focus();
        },
        null);
    } else if (pageKey() == "" || pageKey() == "=") {
      //alert("The password is not set or the room url is incomplete (the part of the url after '#' is missing).");
      $.WarningDialog("The password is not set or the room url is incomplete (the part of the url after '#' is missing).",
        'Missing password',
        function() { //OK
          $("#inputRoomPassword").focus();
        },
        null);              
    } else {
        if ($.trim($("#textMessage").val()) != "") {

            var key = pageKey();

            $.post("sendMessage.php", { roomId: roomId, user: zeroCipher(key, $.trim($("#userName").val())), message: zeroCipher(key, $.trim($("#textMessage").val())) }, function (data) {
                if (data != false) {
                    $("#textMessage").val("");
                    $("#textMessage").focus();
                    getMessages(false);
                } else {
                    // error : the message was not recorded
                    //alert("An error occured... :(");
                    $.ErrorDialog("An error occured... :(",
                      'Sending message',
                      function() { //OK
                        $("#textMessage").focus();
                      },
                      null);                     
                }
            });
        } else {
          $.WarningDialog('Please write a text of the message"!', 'Chat message',
            function() { //OK
              $("#textMessage").focus();
            },
            null);          
        }
    }
}

var isRefreshTitle = false;
var refreshTitleInterval;
var nbIps = 1;

function getMessages(changeTitle) {
    var key = pageKey();
    $.post("getMessages.php", { roomId: roomId, dateLastGetMessages: dateLastGetMessages, nbIps: nbIps }, function (data) {
        if (data == "noRoom") {
            // closed conversation
            $("#chatroom").html("<div id=\"chatroomMsg\"><i>This conversation is over... You should start a new one to keep talking!</i></div>");
            
            //try decode aes key
            var psw = '';
            psw = sjcl.codec.base64.toBits(key);
            $("#inputRoomPassword").val(sjcl.codec.utf8String.fromBits(psw));            
            
            stopTimerCheck();
        } else if (data == "destroyed") {
            // closed conversation
            $("#chatroom").html("<div id=\"chatroomMsg\"><i>This conversation self-destroyed. It was only created for one visitor.</i></div>");
            
            //try decode aes key
            var psw = '';
            psw = sjcl.codec.base64.toBits(key);
            $("#inputRoomPassword").val(sjcl.codec.utf8String.fromBits(psw));            
            
            stopTimerCheck();
        } else if (data != "noNew") {
            if (data.chatLines == undefined) {
                $("#nbUsers").html(data.nbIps);
            } else if (data.chatLines == "") {
                if (key == "" || key == "=") {
                  
                    if (generatePsw) {  //generate random password                      
                      generatePsw = false; //only once
                      var psw = '';                      
                      
                      $.ConfirmDialog('Can you generate a random password for your room?', 'Room password',
                        function() { //YES
                          //psw = sjcl.random.randomWords(8, 0);                    
                          psw = generatePassword_CZECH(0);                      
                          $("#inputRoomPassword").val(psw);                                                                  
                          document.location.hash = "#" + sjcl.codec.base64.fromBits(sjcl.codec.utf8String.toBits(psw), 0);                          
                        },
                        function() {  //NO
                          $("#inputRoomPassword").focus();
                        },
                        null);                                          
                    } else {
                      //set password
                      $("#chatroom").html("<div id=\"chatroomMsg\"><i>The password is not set or the room url is incomplete (the part of the url after '#' is missing).</i></div>");
                      stopTimerCheck();

                      $.ErrorDialog("The password is not set or the room url is incomplete (the part of the url after '#' is missing).",
                        'Missing password',
                        function() { //OK
                          $("#inputRoomPassword").focus();
                        },
                        null);                      
                    }
                                        
                } else {
                  //try decode aes key
                  var psw = '';
                  psw = sjcl.codec.base64.toBits(key);
                  $("#inputRoomPassword").val(sjcl.codec.utf8String.fromBits(psw));
                  
                  $("#chatroom").html("<div id=\"chatroomMsg\"><i>No messages yet...</i></div>");
                }
            } else if (key == "" || key == "=") {
                $("#chatroom").html("<div id=\"chatroomMsg\"><i>The password is not set or the room url is incomplete (the part of the url after '#' is missing).</i></div>");
                stopTimerCheck();
                
                $.ErrorDialog("The password is not set or the room url is incomplete (the part of the url after '#' is missing).",
                  'Missing password',
                  function() { //OK
                    $("#inputRoomPassword").focus();
                  },
                  null);                
            } else {
              
                //try decode aes key
                var psw = '';
                psw = sjcl.codec.base64.toBits(key);
                $("#inputRoomPassword").val(sjcl.codec.utf8String.fromBits(psw));
              
                var hasErrors = false;
                var hasElements = false;
                var chatRoom = $("#chatroom");
                var last_user = "";
                var chatRowClass = "msgOdd";
                chatRoom.html("");

                nbIps = data.nbIps;
                $("#nbUsers").html(data.nbIps);                
                
                for (i = 0; i < data.chatLines.length; i++) {
                    try {
                        var user = zeroDecipher(key, data.chatLines[i].userId);
                        var decryptedMessage = zeroDecipher(key, data.chatLines[i].message);
                        var msg = "";

                        hasElements = true;                                                                       
                        
                        if (vizhash.supportCanvas()) {                            
                            var vhash = vizhash.canvasHash(data.chatLines[i].hash, 15, 10);                            
                            if ((user !== last_user) && (last_user !== "")) {
                              if (chatRowClass === "msgOdd") {
                                chatRowClass = "msgEven";
                              } else {
                                chatRowClass = "msgOdd";
                              }
                            } 
                            msg = msg + "<div id=\"chatroomMsg\" class=\"" + chatRowClass + "\">";
                            msg = msg + "<span class='chathour'>[" + getDateFromTimestamp(data.chatLines[i].date) + "]</span> ";
                            msg = msg + "<span id=\"usr-img-" + i + "\"></span>";
                            msg = msg + " <a onclick='addText(\" @" + htmlEncode(user) + ": \"); return false;' class='userNameLink' href='#'><b>" + htmlEncode(user) + "</b></a>> " + replaceUrlTextWithUrl(htmlEncode(decryptedMessage)).replace(/(?:\r\n|\r|\n)/g, '<br />');
                            msg = msg + "</div>";
                            chatRoom.append(msg);
                            $("#usr-img-" + i).append(vhash.canvas);
                        } else {
                            if ((i % 2) == 0) {
                              msg = msg + "<div id=\"chatroomMsg\" class=\"msgOdd\">";
                            } else {
                              msg = msg + "<div id=\"chatroomMsg\" class=\"msgEven\">";
                            }
                            msg = msg + "<i>[" + data.chatLines[i].date + "]</i> - <a onclick='addText(\" @" + htmlEncode(user) + ": \"); return false;' class='userNameLink' href='#'><b>" + htmlEncode(user) + "</b></a> : ";
                            msg = msg + " <b>" + htmlEncode(user) + "</b>> " + replaceUrlTextWithUrl(htmlEncode(decryptedMessage)).replace(/(?:\r\n|\r|\n)/g, '<br />');
                            msg = msg + "</div>";
                            chatRoom.append(msg);
                        }
                        
                        last_user = user;
                    } catch (e) {
                        hasErrors = true;
                    }
                } //for-loop -> over chatLines
                
                if (!hasElements && hasErrors) {
                    // wrong key error
                    chatRoom.html("<div id=\"chatroomMsg\">The password is wrong or the room url is corrupted. Are you sure that you use the correct password or copied the full URL (with #xxxxxxxxxxxxxxxx-xxxxxxx-xxxxxxxx) ?</div>");
                    stopTimerCheck();
                    
                    $.ErrorDialog("The password is wrong or the room url is corrupted!",
                      'Corupted password',
                      function() { //OK
                        $("#inputRoomPassword").focus();
                      },
                      null);                                         
                } else {
                    var objDiv = document.getElementById("chatroom");
                    objDiv.scrollTop = objDiv.scrollHeight;
                    dateLastGetMessages = data.dateLastGetMessages;

                    if (changeTitle && !isRefreshTitle) {
                        refreshTitleInterval = setInterval(
                            function () {
                                if (document.title == "Private chat room - MyCryptoChatEx by salikovi.cz") {
                                    document.title = "New messages ! - MyCryptoChatEx by salikovi.cz";
                                } else {
                                    document.title = "Private chat room - MyCryptoChatEx by salikovi.cz";
                                }
                            }, 3000);
                        isRefreshTitle = true;
                    }
                }
            }
        } //end - noNew
    });
}

function replaceUrlTextWithUrl(content) {
    var re = /((http|https|ftp):\/\/[\w?=&.\/-;#@~%+-]+(?![\w\s?&.\/;#~%"=-]*>))/ig;
    content = content.replace(re, '<a href="$1" rel="nofollow">$1</a>');
    re = /((magnet):[\w?=&.\/-;#@~%+-]+)/ig;
    content = content.replace(re, '<a href="$1">$1</a>');
    return content;
}

function stopRefreshTitle() {
    if (isRefreshTitle) {
        clearInterval(refreshTitleInterval);
        document.title = "Private chat room - MyCryptoChatEx by salikovi.cz";
        isRefreshTitle = false;
    }
}

function htmlEncode(value) {
    return $('<div/>').text(value).html();
}

var checkIntervalTimer;

function stopTimerCheck() {
    clearInterval(checkIntervalTimer);
}

function getDateFromTimestamp(adate) {
    var date = new Date(adate * 1000);
    
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();
    
    return ((day < 10) ? "0"+day : day)
      + "." + ((month < 10) ? "0"+month : month)
      + "." + year
      + " - " + ((hour < 10) ? "0"+hour : hour)
      + ":" + ((min < 10) ? "0"+min : min)
      + ":" + ((sec < 10) ? "0"+sec : sec);
}

function removeChatroom(withPassword) {
    if (confirm('Are you sure?')) {
        var removePassword = '';
        if (withPassword) {
            var removePassword = prompt("Please enter the password to remove the chat room", "");
        }
        $.post("removeChatroom.php", { roomId: roomId, removePassword: removePassword }, function (data) {
            if (data == "error") {
                //alert('An error occured');
                $.ErrorDialog("An error occured",
                  'Removing chatroom',
                  function() { //OK
                    $("#textMessage").focus();
                  },
                  null);                
            } else if (data == "wrongPassword") {
                //alert('Wrong password');
                $.ErrorDialog("Wrong password",
                  'Removing chatroom',
                  function() { //OK
                    $("#textMessage").focus();
                  },
                  null);                 
            } else if (data == "removed") {
                //alert('The chat room has been removed.');
                $.InfoDialog("The chat room has been removed.",
                  'Removing chatroom',
                  function() { //OK
                    //
                  },
                  null);                 
                window.location = 'index.php';
            }
        });
    }
}

function addText(text) {
    var editor = $('#textMessage');
    var value = editor.val();
    editor.val("");
    editor.focus();
    editor.val(value + text);
}

function setRoomPassword() {
  $.ConfirmDialog('Do you realy want to set the new password?', 'Change room password',
    function() { //YES     
      var psw = $('#inputRoomPassword').val();      
      document.location.hash = "#" + sjcl.codec.base64.fromBits(sjcl.codec.utf8String.toBits(psw), 0);              
      
      dateLastGetMessages = Math.round(new Date().getTime()/1000) - (60 * 60 * 24 * 365);          
      setupMessageChecking();
    },
    function() {  //NO
      //
    },
    null);
    
  /*
  var answer = confirm('Do you realy want to set the new password?');
  if (answer) {
    var psw = $('#inputRoomPassword').val();  
    document.location.hash = "#" + sjcl.codec.base64.fromBits(sjcl.codec.utf8String.toBits(psw), 0);  
    
    dateLastGetMessages = Math.round(new Date().getTime()/1000) - (60 * 60 * 24 * 365);    
    setupMessageChecking();
  }
  */
}

function setupMessageChecking() {
  getMessages(false);

  // try to get new messages every 15 seconds
  checkIntervalTimer = setInterval("getMessages(true)", 15000);

  $('body').on('mousemove', stopRefreshTitle);  
}

$.ConfirmDialog = function (message, title, callbackYes, callbackNo, callbackArgument) {
  /// <summary>
  ///     Generic confirmation dialog.
  ///
  ///     Usage:
  ///         $.ConfirmDialog('Do you want to continue?', 'Continue Title', function() { alert('yes'); }, function() { alert('no'); }, null);
  ///         $.ConfirmDialog('Do you want to continue?', 'Continue Title', function(arg) { alert('yes, ' + arg); }, function(arg) { alert('no, ' + arg); }, 'please');
  ///</summary>
  ///<param name="message" type="String">
  ///     The string message to display in the dialog.
  ///</param>
  ///<param name="title" type="String">
  ///     The string title to display in the top bar of the dialog.
  ///</param>
  ///<param name="callbackYes" type="Function">
  ///     The callback function when response is yes.
  ///</param>
  ///<param name="callbackNo" type="Function">
  ///     The callback function when response is no.
  ///</param>
  ///<param name="callbackNo" type="Object">
  ///     Optional parameter that is passed to either callback function.
  ///</param>

  if ($("#modalConfirmDialog").length == 0)
      $('body').append('<div id="modalConfirmDialog"></div>');

  var dlg = $("#modalConfirmDialog")
      .html(message)
      .dialog({
          autoOpen: false, // set this to false so we can manually open it
          dialogClass: "confirmScreenWindow", //deprecated
          closeOnEscape: true,
          draggable: false,
          width: 460,
          minHeight: 50,
          modal: true,
          resizable: false,
          title: title,
          buttons: {
              Yes: function () {
                  if (callbackYes && typeof (callbackYes) === "function") {
                      if (callbackArgument == null) {
                          callbackYes();
                      } else {
                          callbackYes(callbackArgument);
                      }
                  }

                  $(this).dialog("close");
              },

              No: function () {
                  if (callbackNo && typeof (callbackNo) === "function") {
                      if (callbackArgument == null) {
                          callbackNo();
                      } else {
                          callbackNo(callbackArgument);
                      }
                  }

                  $(this).dialog("close");
              }
          }
      });
  dlg.dialog("open");
};

$.InfoDialog = function (message, title, callbackOk, callbackArgument) {
  if ($("#modalInformationDialog").length == 0)
      $('body').append('<div id="modalInformationDialog"></div>');
      
  var dlg = $("#modalInformationDialog")
      .html(message)
      .dialog({
          autoOpen: false, // set this to false so we can manually open it
          //dialogClass: "ui-state-warning",
          closeOnEscape: true,
          draggable: false,
          width: 460,
          minHeight: 50,
          modal: true,
          resizable: false,
          title: title,
          buttons: {
              Ok: function () {
                  if (callbackOk && typeof (callbackOk) === "function") {
                      if (callbackArgument == null) {
                          callbackOk();
                      } else {
                          callbackOk(callbackArgument);
                      }
                  }

                  $(this).dialog("close");
              }
          }
      });
  dlg.dialog("open");
}

$.WarningDialog = function (message, title, callbackOk, callbackArgument) {
  if ($("#modalWarningDialog").length == 0)
      $('body').append('<div id="modalWarningDialog"></div>');
      
  var dlg = $("#modalWarningDialog")
      .html(message)
      .dialog({
          autoOpen: false, // set this to false so we can manually open it
          dialogClass: "ui-state-warning",
          closeOnEscape: true,
          draggable: false,
          width: 460,
          minHeight: 50,
          modal: true,
          resizable: false,
          title: title,
          buttons: {
              Ok: function () {
                  if (callbackOk && typeof (callbackOk) === "function") {
                      if (callbackArgument == null) {
                          callbackOk();
                      } else {
                          callbackOk(callbackArgument);
                      }
                  }

                  $(this).dialog("close");
              }
          }
      });
  dlg.dialog("open");
}

$.ErrorDialog = function (message, title, callbackOk, callbackArgument) {
  if ($("#modalErrorDialog").length == 0)
      $('body').append('<div id="modalErrorDialog"></div>');
      
  var dlg = $("#modalErrorDialog")
      .html(message)
      .dialog({
          autoOpen: false, // set this to false so we can manually open it
          dialogClass: "ui-state-error",
          closeOnEscape: true,
          draggable: false,
          width: 460,
          minHeight: 50,
          modal: true,
          resizable: false,
          title: title,
          buttons: {
              Ok: function () {
                  if (callbackOk && typeof (callbackOk) === "function") {
                      if (callbackArgument == null) {
                          callbackOk();
                      } else {
                          callbackOk(callbackArgument);
                      }
                  }

                  $(this).dialog("close");
              }
          }
      });
  dlg.dialog("open");
}

$(function () {

    $("#glyphicon-eye-open").mousedown(function(){
      $("#inputRoomPassword").prop('type', 'text');      
    }).mouseup(function(){
        $("#inputRoomPassword").prop('type', 'password');        
    }).mouseout(function(){
      $("#inputRoomPassword").prop('type', 'password');      
    });
      
    setupMessageChecking(); //star chat
});