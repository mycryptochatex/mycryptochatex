<?php
  $inc_dir = '../../inc/';
  
  require $inc_dir . 'constants.php';
  require $inc_dir . 'classes.php';
  require $inc_dir . 'functions.php';
  require $inc_dir . 'dbmanager.php';
  
  require 'conf.php';
  require 'classes.php';
  require 'functions.php';
?>

<?php
  $ret = array(); //response
  try {
  // ---------------------------------------------------------------------------
  

  // #0 - header
  $ret['header'] = "MyCryptoChatEx by salikovi.cz";   
  
  
  // #1 - settings tests
  $sqliAvailable = true;
  $showContent = true;
  $configIncluded = false;   
  
  check_server($ret,
    $sqliAvailable,
    $showContent,
    $configIncluded      
  );  
  
  if (!$showContent) {
    throw new Exception(); //break try
  }
   
  
  // #2 - add usage stats
  add_usage_stats($ret);  
  
    
  // #3 - decode parameters
  $par = array();
  $par[] = parse_get_params();
  $par[] = parse_post_params();
  
  
  // #4 - do job
  $group = nvl($par, 'group', 'room'); 
  if ($group == 'room') {    
    $action = nvl($par, 'action', 'list');   
    switch ($action) {
      
      case 'list':
        get_room_list($ret);
        break;

      case 'create':
          $name = nvl($par, 'name', 'Untitled room');
          $hidden = nvl($par, 'hidden', 0);
          $self_destroy = nvl($par, 'self_destroy', 0); //if more than me and 1 visitor in the room
          $minutes_to_live = nvl($par, 'minutes_to_live', 0);
          $is_removable = nvl($par, 'removable', 0);
          $remove_password = nvl($par, 'remove_pass', null);
        break;      
      
      case 'read':
        $room_id = nvl($par, 'room_id', -1);
        $last_msg_check = nvl($par, 'last_msg_check', 0);
        $user_count = nvl($par, 'usr_cnt', 0);        
        break;
      
      case 'write':
        $room_id = nvl($par, 'room_id', -1);
        $user_name = nvl($par, 'user_name', null);
        $msg = nvl($par, 'msg', null);
        break;     
      
      case 'delete':
        $room_id = nvl($par, 'room_id', -1);
        $remove_password = nvl($par, 'remove_pass', null);
        break;
      
      default:
        get_room_list($ret);
    } //end-switch
  } //end-group-room
  
  
  // ---------------------------------------------------------------------------  
  } catch (Exception $e) {
    logException($e);
    
    add_error($ret, "Error: unexpected",
      "Unexpected error during server settings checking. " . $e->getMessage()
    );    
  };
    
  echo json_encode($ret);  
  exit();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Home - MyCryptoChatEx by salikovi.cz</title>    
    
    <link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />    
    
    <meta name="viewport" content="width=device-width" />
    <link href="styles/myCryptoChat.css" rel="stylesheet" />    
    <script src="scripts/modernizr.min.js"></script>
    <link href="styles/jquery.dataTables.css" rel="stylesheet" />    
</head>
<body>
    <?php         
    
    if($showContent) {
    ?>
    <noscript>
        This website needs JavaScript activated to work. 
              <style>
                  div {
                      display: none;
                  }
              </style>
    </noscript>
    <header>
        <div class="content-wrapper">
            <div class="float-left">
                <p class="site-title"><a href="index.php">MyCryptoChatEx</a></p>
            </div>
            <div class="float-right">
                <section id="login">
                </section>
                <nav>
                    <ul id="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="stats.php">Stats</a></li>
                        <li><a href="about.php">About</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <div id="body">
        <section class="content-wrapper main-content clear-fix">

          <h2>MyCryptoChatEx</h2>
          <div class="mb50">Chat with friends without anyone spying on what you say!</div>

          <div class="chatRoomList mb50">
            <table id="chatRoomTable" class="hover compact" cellspacing="0" width="100%">
                  <thead>
                      <tr>                      
                          <th>Chat room name</th>                          
                          <th>Users count</th>
                          <th>Created</th>
                          <th>Expiration</th>
                          <th>Last message</th>
                          <th>Link</th>
                      </tr>
                  </thead>
                  <tfoot>
                      <tr>
                          <th>Chat room name</th>                          
                          <th>Users count</th>
                          <th>Created</th>
                          <th>Expiration</th>
                          <th>Last message</th>
                          <th>Link</th>
                      </tr>
                  </tfoot>
              </table>            
          </div>
            
          <form method="POST" action="newroom.php">
            <label for="roomName">Name of the chat room:</label>
            <input type="text" name="roomName" value="<?php echo $dbManager->GetNextRoomName(); ?>" />
            <br />
            
            <label for="nbMinutesToLive">Lifetime of the chat room:</label>
            <select id="nbMinutesToLive" name="nbMinutesToLive">
              <?php foreach ($allowedTimes as $minutes => $label) { ?>
                  <option value="<?php echo $minutes; ?>"><?php echo $label; ?></option>
              <?php } ?>
            </select><br />
            <br />
				
            <input type="checkbox" name="isRemovable" id="isRemovable" value="true" onchange="if(this.checked) { $('#divRemovePassword').show(); } else { $('#divRemovePassword').hide(); }" />
            <label for="isRemovable" class="labelIndex">Is removable</label>
            <br />
            
            <div id="divRemovePassword">
              <br />
              <label for="removePassword">Password to remove:</label>
              <input type="password" name="removePassword" value="" />
            </div>
            <br />
				
            <input type="checkbox" name="selfDestroys" id="selfDestroys" value="true" />
            <label for="selfDestroys" class="labelIndex">Self-destroys if more than one visitor</label>            
            <br />				
            <br />
            
            <input type="checkbox" name="hiddenRoom" id="hiddenChatRoom" value="true" />
            <label for="hiddenChatRoom" class="labelIndex">Chat room will not be visible in rooms list</label>            
            <br />				
            <br />            
            
            <input type="submit" value="Create a new chat room" />
          </form>
            
        </section>
    </div>
    <footer>
        <div class="content-wrapper">
            <div class="float-left">
                <p>&copy; 2016 - MyCryptoChatEx <?php echo MYCRYPTOCHAT_VERSION; ?> by salikovi.cz</p>
            </div>
        </div>
    </footer>
    
    <script src="scripts/jquery.min.js"></script>
    <script src="scripts/jquery.dataTables.min.js"></script>
    
    <script type="text/javascript">
      $('#chatRoomTable').DataTable( {
        //"processing": true,
        //"serverSide": true,
        "ajax": "roomslist.php"
      } );
    </script>    
    
    <?php } //if-showcontent ?>
</body>
</html>
