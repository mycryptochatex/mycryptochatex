<?php

  function nvl() {
    $ret = null;
    
    $arg = array();    
    for ($i = 0; $i < func_num_args(); $i++) {
      $arg[$i] = func_get_arg($i);
    }    
    
    try {      
      
      if (count($arg) == 3) {
        if (is_array($arg[0])) {
          //array
          if ( !empty($arg[0][$arg[1]]) ) {
            $ret = $arg[0][$arg[1]];
          } else {
            $ret = $arg[2];
          }
        } else if (is_object($arg[0])) {
          //object
          if ( !empty($arg[0]->$arg[1]) ) {
            $ret = $arg[0]->$arg[1];
          } else {
            $ret = $arg[2];
          }
        } else if ( !empty($arg[0]) ) {
          $ret = $arg[1];
        } else {
          $ret = $arg[2];
        }
      } else if (count($arg) == 2) {
        //normal variable
        if ( !empty($arg[0]) ) {
          $ret = $arg[0];
        } else {
          $ret = $arg[1];            
        }        
      }
      
    } catch (Exception $e) {
      $ret = null;
    }    
    return $ret;
  }

  function parse_uri_params() {
    $arr = array();
    
    //params in uri
    $uri = $_SERVER['REQUEST_URI'];    
    $api_root_pos = strpos($uri, API_URI_ROOT);    
    
    if ($api_root_pos > 0) {
      $api_root_pos = $api_root_pos + strlen(API_URI_ROOT) - 1;
      $uri = trim(substr($uri, $api_root_pos));
      $par = explode('/', $uri);
      
      for ($i=0; ($i < count($arr)) && ($i < 2); $i++) {
        if ($i == 0) {
          $arr['group'] = $par[$i];
        } else if ($i == 1) {
          
          $arr['action'] = $par[$i];            
          switch($arr['action']) {
            case 'create':
              for ($j=2; ($j < count($arr)) && ($j <= ($i+6)); $j++) {
                switch ($j-1) {
                  case 1: 
                    $arr['minutes_to_live'] = $par[$j];
                    break;
                  case 2: 
                    $arr['self_destroy'] = $par[$j];
                    break;
                  case 3: 
                    $arr['removable'] = $par[$j];
                    break;
                  case 4: 
                    $arr['remove_pass'] = $par[$j];
                    break;
                  case 5: 
                    $arr['hidden'] = $par[$j];
                    break;
                  case 6: 
                    $arr['name'] = $par[$j];
                    break;                    
                } //switch-create-params
              } //for-create-params
              break;      

            case 'read':
              for ($j=2; ($j < count($arr)) && ($j <= ($i+3)); $j++) {
                switch ($j-1) {
                  case 1: 
                    $arr['room_id'] = $par[$j];
                    break;
                  case 2: 
                    $arr['last_msg_check'] = $par[$j];
                    break;
                  case 3: 
                    $arr['usr_cnt'] = $par[$j];
                    break;
                } //switch-read-params
              } //for-read-params
              break;

            case 'write':
              for ($j=2; ($j < count($arr)) && ($j <= ($i+3)); $j++) {
                switch ($j-1) {
                  case 1: 
                    $arr['room_id'] = $par[$j];
                    break;
                  case 2: 
                    $arr['user_name'] = $par[$j];
                    break;
                  case 3: 
                    $arr['msg'] = $par[$j];
                    break;
                } //switch-read-params
              } //for-read-params
              break;     

            case 'delete':
              for ($j=2; ($j < count($arr)) && ($j <= ($i+2)); $j++) {
                switch ($j-1) {
                  case 1: 
                    $arr['room_id'] = $par[$j];
                    break;
                  case 2: 
                    $arr['remove_pass'] = $par[$j];
                    break;
                } //switch-delete-params
              } //for-delete-params
              break;  

          } //end-switch-action
            
        } //end-if-main params
      } //end-for over main params
      
    }    
    return $arr;
  }

  function parse_post_params() {
    $arr = null;
    
    if (!empty($_POST["jdata"])) {
      $par = json_decode($_POST['jdata']);
      if (!empty($par)) {        
        $arr = $par;        
      }
    }
    return $arr;
  }
  
  function add_error(&$ret, $error, $error_desc) {
    if (!isset($ret['errors'])) {      
      $ret['errors'] = array();
    }    
    
    $err = new stdClass();
    $err->error = $error;
    $err->description = $error_desc;
    
    $ret['errors'][] = $err;
    
    return;
  }

  function add_usage_stats(&$ret) {
    $dbManager = new DbManager();
    
    $time = $_SERVER['REQUEST_TIME'];
    $dbManager->CleanChatrooms($time);
   
    $stats = new ApiUsageStats();
    $stats->numChatRooms = $dbManager->GetNbChatRooms();
    $stats->numChatMessages = $dbManager->GetNbMessages();
    $stats->numActiveUsers = $dbManager->GetNbActiveUsers();

    $ret['stats'] = $stats;
    
    return;
  }
  
  function check_server(&$ret, &$sqliAvailable, &$showContent, &$configIncluded) {    
    if(is_readable(ROOT_DIR . CONFIG_FILE_NAME)) {   
      include ROOT_DIR . CONFIG_FILE_NAME;
      $configIncluded = true;
    } else {
      $showContent = false;      
      add_error($ret, "Error: missing inc/conf.php",
        "MyCryptoChatEx can't read the configuration file."
        ." Copy 'inc/conf.template.php' into 'inc/conf.php', and don't forget to 'customize it'"
      );
    }  

    if (!extension_loaded('PDO')) {
      $showContent = false;
      $sqliAvailable = false;

      add_error($ret, "Error: PDO missing",
        "The PDO module is missing. Please add it and load it to make this website work."
      );
    }    

    if (!extension_loaded('PDO_SQLITE')) {
      $showContent = false;
      $sqliAvailable = false;

      add_error($ret, "Error: PDO SQLite missing",
        "The PDO SQLite module is missing. Please add it and load it to make this website work."
      );
    }   

    if ($sqliAvailable) {
      //create clean database
      $dbManager = new DbManager();      
      if (!file_exists(ROOT_DIR . DB_FILE_NAME)) {
        $dbManager->RecreateDatabase();
      }     

      //db check
      if(!is_writable(ROOT_DIR . DB_FILE_NAME)) {
        $showContent = false;
        
        add_error($ret, "Error: database access",
          "MyCryptoChatEx can't edit the database file."
          ." Please give all rights to the apache (or current) user on the 'chatrooms.sqlite' file."
        );
      } //db check
    } //PDO extension is available  

    if(!file_exists(ROOT_DIR . LOGS_FILE_NAME)) {
      touch(ROOT_DIR . LOGS_FILE_NAME);
    }    
    if(!is_writable(ROOT_DIR . LOGS_FILE_NAME)) {
      $showContent = false;
      
      add_error($ret, "Error: logs file access",
        "MyCryptoChatEx can't edit the logs file."
        ." Please give all rights to the apache (or current) user on the 'logs.txt' file."
      );      
    }  

    if (version_compare(phpversion(), '5.3.29', '<')) {
      $showContent = false;
      
      add_error($ret, "Error: php version",
        "The version of php is " . phpversion() ." and this is too low."
        ." You need at least PHP 5.4 to run this website."
      );      
    }   

    if ( ($configIncluded === true) && (SEED == 'f-rjng24!1r5TRHHgnjrt') ) {
      $showContent = false;
      
      add_error($ret, "Error: the seed was not modified",
        "The seed that is used to do a better hashing for users is still 'f-rjng24!1r5TRHHgnjrt'"
        ." Please modify its value in 'inc/conf.php'. You could may be use '" . randomString(20) . "', or another."
      );       
    } 
    
    return;
  }  
  
  function get_room_list(&$ret) {    
    $dbManager = new DbManager();
    $rooms = $dbManager->GetRoomList();    
    
    $api_room = new ApiChatRoomForList();
    $room_list = array();
    
    foreach ($rooms as $room) {  
      $api_room->id = $room->id;
      $api_room->name = $room->name;
      $api_room->userCount = count(json_decode($room->users, true));
      $api_room->dateCreation = date('d.m.Y H:i:s', $room->dateCreation);
      $api_room->dateLastNewMessage = date('d.m.Y H:i:s', $room->dateLastNewMessage);
      if ($room->dateEnd != 0) {
        $api_room->dateEnd = date('d.m.Y H:i:s', $room->dateEnd);
      } else {
        $api_room->dateEnd = date('d.m.Y H:i:s', $room->dateLastNewMessage + (DAYS_TO_DELETE_IDLE_CHATROOM * 24 * 60 * 60));
      }
      
      $room_list[] = $api_room;
    }
    
    $ret['rooms'] = $room_list;
    return;
  }

?>