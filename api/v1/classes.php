<?php

  class ApiUsageStats {
    public $numChatRooms;
    public $numChatMessages;
    public $numActiveUsers;
  }

	class ApiChatRoomForList {
		public $id;
    public $name;
		public $userCount;    
		public $dateCreation;
		public $dateLastNewMessage;
		public $dateEnd;
	}

	class ApiChatRoom {
		public $id;
    public $name;
		public $userCount;    
		public $dateCreation;
		public $dateLastNewMessage;
		public $dateEnd;
    //
		public $noMoreThanOneVisitor;  //more than 1 visitors will destroy the chatroom
		public $isRemovable;
		public $removePassword;
    public $hidden;
	}    
  /*
   * 
      $api_room->isRemovable = (is_null($room->isRemovable) ? 0 : $room->isRemovable);
      $api_room->removePassword = (is_null($room->removePassword) ? '' : $room->removePassword);
      $api_room->noMoreThanOneVisitor = (is_null($room->noMoreThanOneVisitor) ? 0 : $room->noMoreThanOneVisitor);
      $api_room->hidden = (is_null($room->hidden) ? 0 : $room->hidden);
   *       
   */
  
  class ApiChatMessages {
    public $roomId;
    public $chatMessages;
  }
  
?>
