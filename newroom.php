<?php
require 'inc/conf.php';
require 'inc/constants.php';
require 'inc/init.php';
require 'inc/functions.php';
require 'inc/classes.php';
require 'inc/dbmanager.php';

if(array_key_exists($_POST['nbMinutesToLive'], $allowedTimes)) {
    $nbMinutesToLive = $_POST['nbMinutesToLive'];
} else {
    exit('cheater');
}

$time = $_SERVER['REQUEST_TIME'];
//$time = time();

$selfDestroys = isset($_POST['selfDestroys']) && $_POST['selfDestroys'] == 'true';

$isRemovable = isset($_POST['isRemovable']) && $_POST['isRemovable'] == 'true';
$removePassword = $_POST['removePassword'];

$hiddenRoom = isset($_POST['hiddenRoom']) && $_POST['hiddenRoom'] == 'true';

$dbManager = new DbManager();

if ( isset($_POST['roomName']) ) {
  $roomName = $_POST['roomName'];
} else {
  $nbChatrooms = $dbManager->GetNbChatRooms() + 1;
  $roomName = sprintf('Room %d', $nbChatrooms);
}

$userHash = getHashForIp();

// we generate a random key
$key = randomString(20);

// we create the chat room object
$chatRoom = new ChatRoom();
$chatRoom->id = $key;
$chatRoom->name = $roomName;
$chatRoom->dateCreation = $time;
$chatRoom->dateLastNewMessage = $time;
$chatRoom->dateEnd = $nbMinutesToLive != 0 ? $time + ($nbMinutesToLive * 60) : 0;
$chatRoom->noMoreThanOneVisitor = $selfDestroys;
$chatRoom->isRemovable = $isRemovable;
$chatRoom->removePassword = $removePassword;
$chatRoom->hidden = $hiddenRoom;
$chatRoom->userId = $userHash;

$chatUser = array();
$chatUser['id'] = $userHash;
$chatUser['dateLastSeen'] = $time;
array_push($chatRoom->users, $chatUser);

// we delete old chatrooms
$dbManager->CleanChatrooms($time);

// we save the chat room in sqlite
$dbManager->CreateChatroom($chatRoom);


//header('Location: chatroom.php?id=' . $key);
//use browser redirection (because of generate password token in url)

?>

<html>
  <header>Redirection to new chatroom ...</header>
  <body>
    
    <form method="POST" action="chatroom.php?id=<?php echo $key; ?>" name="redirectForm">
      <input type="hidden" name="<?php echo GENERATE_PASSWORD_TOKEN_NAME; ?>" value="<?php echo GENERATE_PASSWORD_TOKEN_VALUE; ?>">      
    </form>
    
    <script type='text/javascript'>
      document.redirectForm.submit();
    </script>    
  </body>>
</html>


<?php
?>