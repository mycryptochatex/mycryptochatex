<?php

define('GENERATE_PASSWORD_TOKEN_NAME', 'psw');
define('GENERATE_PASSWORD_TOKEN_VALUE', 'generate');

$dir = dirname(__FILE__);
$root_dir = preg_replace('/inc$/', '', $dir); //remove INC dir
define('ROOT_DIR', $root_dir);

define('DB_FILE_NAME', 'db/chatrooms_8d4f5ds6d.sqlite');
define('LOGS_FILE_NAME', 'db/logs.txt');
define('CONFIG_FILE_NAME', 'inc/conf.php');

define('MYCRYPTOCHAT_VERSION', 'v1.1.2');

?>