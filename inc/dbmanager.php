<?php
class DbManager {
    private $db = null;
    
    function DbManager() {
      if (file_exists(ROOT_DIR . DB_FILE_NAME)) {
        try {
            $this->db = new PDO('sqlite:' . ROOT_DIR . DB_FILE_NAME);
            $this->db->setAttribute(PDO::ATTR_PERSISTENT, true /*, PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION*/);
            $this->db->exec('PRAGMA temp_store = MEMORY; PRAGMA synchronous=OFF;');
        } catch (Exception $e) {
            logException($e);
            die('Error: database error.');
        }
      } else {
        logDebug('Database ' . ROOT_DIR . DB_FILE_NAME . ' does not exists!');
        $this->db = null;
      }
    }
    
    function CreateChatroom($chatRoom) {
        if(is_null($chatRoom)) {
            die('Parameter error.');   
        }
        try {
			$query = 'INSERT INTO ChatRoom ( Id, Name, DateCreation, DateLastNewMessage, ConnectedUsers, DateEnd, NoMoreThanOneVisitor, IsRemovable, RemovePassword, Hidden, UserHash )';
			$query .= ' VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
			
			$req = $this->db->prepare($query);

			$req->execute(array(
        $chatRoom->id,
        $chatRoom->name,
        $chatRoom->dateCreation,
        $chatRoom->dateLastNewMessage,
        json_encode($chatRoom->users),
        $chatRoom->dateEnd,
        $chatRoom->noMoreThanOneVisitor ? 1 : 0,
        $chatRoom->isRemovable ? 1 : 0,
        $chatRoom->removePassword,
        $chatRoom->hidden ? 1 : 0,
        $chatRoom->userId
      ));
            if($this->db->errorCode() != '00000') 
            {
                die('Error: database error.');
            }
        }
        catch (Exception $e) 
        {
            logException($e);
            die('Error: database error.');
        }
    }
    
    function CleanChatrooms($time) {
      try {
        
        $query = 'DELETE FROM ChatMessage WHERE IdChatRoom IN (SELECT Id FROM ChatRoom WHERE ( DateEnd <> 0 AND DateEnd < ? ) OR DateLastNewMessage < ?)';
        $idleTime = $time - (DAYS_TO_DELETE_IDLE_CHATROOM * 24 * 60 * 60);
        $req = $this->db->prepare($query);
        $req->execute(array($time, $idleTime));

        $query = 'DELETE FROM ChatRoom WHERE ( DateEnd <> 0 AND DateEnd < ? ) OR DateLastNewMessage < ?';
        $req = $this->db->prepare($query);
        $req->execute(array($time, $idleTime));
        
      } catch (Exception $e) {
          logException($e);
          die('Error: database error.');
      }
    }
    
    function DeleteChatroom($chatRoomId) {
        try {
            $query = 'DELETE FROM ChatMessage WHERE IdChatRoom = ?';
            
            $req = $this->db->prepare($query);
            
            $req->execute(array($chatRoomId));
            
            $query = 'DELETE FROM ChatRoom WHERE Id = ?';
            
            $req = $this->db->prepare($query);
            
            $req->execute(array($chatRoomId));
        }
        catch (Exception $e) 
        {
            logException($e);
            die('Error: database error.');
        }
    }
    
    function GetChatroom($id) {
        if(is_null($id) || $id == '') {
            return null;    
        }
        
        try {            
            $query = 'SELECT Id, Name, DateCreation, DateLastNewMessage, ConnectedUsers, DateEnd, NoMoreThanOneVisitor, IsRemovable, RemovePassword, UserHash FROM ChatRoom WHERE Id = ?';
            
            $req = $this->db->prepare($query);
            
            $req->execute(array($id));
            
            $result = $req->fetchAll();
            
            if(is_null($result) || count($result) != 1) {
                return null;
            }
            
            $resultRow = $result[0];
            
            $chatRoom = new ChatRoom;
            $chatRoom->id = $resultRow['Id'];
            $chatRoom->name = $resultRow['Name'];
            $chatRoom->dateCreation = $resultRow['DateCreation'];
            $chatRoom->dateLastNewMessage = $resultRow['DateLastNewMessage'];
            $chatRoom->users = json_decode($resultRow['ConnectedUsers'], true);
            $chatRoom->dateEnd = $resultRow['DateEnd'];
            $chatRoom->noMoreThanOneVisitor = $resultRow['NoMoreThanOneVisitor'] == 1;
            $chatRoom->isRemovable = $resultRow['IsRemovable'] == 1;
            $chatRoom->removePassword = $resultRow['RemovePassword'];
            $chatRoom->userId = $resultRow['UserHash'];
            
            return $chatRoom;
        }
        catch (Exception $e) 
        {
            logException($e);
            die('Error: database error.');
        }
    }
    
    function UpdateChatRoomUsers($chatRoom) {
        try {
            $query = 'UPDATE ChatRoom SET ConnectedUsers = ? WHERE Id = ?';
            
            $req = $this->db->prepare($query);
            
            $req->execute(array(json_encode($chatRoom->users), $chatRoom->id));
            
            if($this->db->errorCode() != '00000') 
            {
                die('Error: database error.');
            }
        }
        catch (Exception $e) 
        {
            logException($e);
            die('Error: database error.');
        }
    }
    
    function GetLastMessages($chatRoomId, $nbMessages) {
      try {
        $query = 'SELECT Message, Hash, User, Date FROM ChatMessage WHERE IdChatRoom = ? ORDER BY Date DESC LIMIT ?';            
        $req = $this->db->prepare($query);            
        $req->execute(array($chatRoomId, $nbMessages));

        $messages = array();

        while ($line = $req->fetch()) { 
            $chatMessage = new ChatMessage;
            $chatMessage->message = $line['Message'];
            $chatMessage->hash = $line['Hash'];
            $chatMessage->userId = $line['User'];
            $chatMessage->date = $line['Date'];
            array_unshift($messages, $chatMessage);
        } 

        return $messages;
        
      } catch (Exception $e)  {
        logException($e);
        die('Error: database error.');
      }
    }
    
    function UpdateChatRoomDateLastMessage($chatRoomId, $time) {
        try {
            $query = 'UPDATE ChatRoom SET DateLastNewMessage = ? WHERE Id = ?';
            
            $req = $this->db->prepare($query);
            
            $req->execute(array($time, $chatRoomId));
            
            if($this->db->errorCode() != '00000') 
            {
                die('Error: database error.');
            }
        }
        catch (Exception $e) 
        {
            logException($e);
            die('Error: database error.');
        }
    }
    
    function AddMessage($chatRoomId, $message, $userMessage, $hash, $time) {
        try {
            $query = 'INSERT INTO ChatMessage ( IdChatRoom, Message, Hash, User, Date ) VALUES ( ?, ?, ?, ?, ? )';
            
            $req = $this->db->prepare($query);
            
            $req->execute(array($chatRoomId, $message, $hash, $userMessage, $time));
            
            if($this->db->errorCode() != '00000') 
            {
                die('Error: database error.');
            }
        }
        catch (Exception $e) 
        {
            logException($e);
            die('Error: database error.');
        }
    }
    
    function GetNbChatRooms() {
        try {
            $query = 'SELECT COUNT(Id) FROM ChatRoom';
            
            $req = $this->db->prepare($query);
            
            $req->execute();
            
            $result = $req->fetchAll();
            
            if(is_null($result) || count($result) != 1) {
                return -1;
            }
            
            $resultRow = $result[0];
            
            return $resultRow[0];
        }
        catch (Exception $e) 
        {
            logException($e);
            die('Error: database error.');
        }
    }
    
    function GetNbMessages() {
        try {
            $query = 'SELECT COUNT(IdChatRoom) FROM ChatMessage';
            
            $req = $this->db->prepare($query);
            
            $req->execute();
            
            $result = $req->fetchAll();
            
            if(is_null($result) || count($result) != 1) {
                return -1;
            }
            
            $resultRow = $result[0];
            
            return $resultRow[0];
        }
        catch (Exception $e) 
        {
            logException($e);
            die('Error: database error.');
        }
    }   
    
    function GetNextRoomName () {
      $nbChatrooms = $this->GetNbChatRooms() + 1;
      $roomName = sprintf('Room %d', $nbChatrooms);
      return $roomName;
    }
        
    function RecreateDatabase() {
      //close connection
      if ($this->db != null) {
        $this->db = null;
      }
      
      //delete old db file
      if (file_exists(ROOT_DIR . DB_FILE_NAME)) {
        unlink(ROOT_DIR . DB_FILE_NAME);
      }
      
      //create new database file
      try {
          $this->db = new PDO('sqlite:' . ROOT_DIR . DB_FILE_NAME);
          $this->db->setAttribute(PDO::ATTR_PERSISTENT, true /*, PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION*/);
          $this->db->exec('PRAGMA temp_store = MEMORY; PRAGMA synchronous=OFF;');
          
          $this->db->exec('CREATE TABLE ChatMessage (
            IdChatRoom TEXT  NOT NULL,
            Message TEXT  NOT NULL,
            Hash TEXT  NOT NULL,
            User TEXT  NOT NULL,
            Date INTEGER  NOT NULL
          )');

          $this->db->exec('CREATE TABLE ChatRoom (
            Id TEXT  UNIQUE NOT NULL,
            Name TEXT  NOT NULL,
            DateCreation INTEGER  NOT NULL,
            DateLastNewMessage INTEGER  NULL,
            ConnectedUsers TEXT  NULL,
            DateEnd INTEGER  NULL,
            NoMoreThanOneVisitor INTEGER  NULL,
            UserHash TEXT  NOT NULL,
            IsRemovable INTEGER  NOT NULL,
            RemovePassword TEXT  NULL,
            Hidden INTEGER NOT NULL
          )');
                    
          $this->db->exec('CREATE TABLE ChatUser (
            Id INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL,
            Login VARCHAR(100)  UNIQUE NOT NULL,
            Password VARCHAR(32)  NOT NULL,
            Role INTEGER DEFAULT \'10\' NOT NULL
          )');
          //insert {user:root, password:root}
          $this->db->exec("INSERT INTO ChatUser (Id, Login, Password, Role) VALUES (null, 'root', '63a9f0ea7bb98050796b649e85481845', 0);");
                    
      } catch (Exception $e) {
          logException($e);
          die('Error: database error.');
      }           
    }
    
    function GetRoomList() {
        try {
          $query = 'SELECT Id, Name, ConnectedUsers, DateCreation, DateLastNewMessage, DateEnd '
                 . 'FROM ChatRoom '
                 . 'WHERE (Hidden = ?) '
                 . '  AND ((DateEnd = 0) OR (DateEnd > ?)) '
                 . '  AND (DateLastNewMessage > ?) '
                 . 'ORDER BY DateLastNewMessage DESC';          
          $req = $this->db->prepare($query);
          $time = $_SERVER['REQUEST_TIME'];
          $idleTime = $time - (DAYS_TO_DELETE_IDLE_CHATROOM * 24 * 60 * 60);          
          $req->execute(array('0', $time, $idleTime));

          $rooms = array();
          
          while ($line = $req->fetch()) { 
              $chatRoom = new ChatRoom;
              $chatRoom->id = $line['Id'];
              $chatRoom->name = $line['Name'];
              $chatRoom->users = $line['ConnectedUsers'];
              $chatRoom->dateCreation = $line['DateCreation'];
              $chatRoom->dateLastNewMessage = $line['DateLastNewMessage'];
              $chatRoom->dateEnd = $line['DateEnd'];
              array_unshift($rooms, $chatRoom);
          } 

          return $rooms;
        } catch (Exception $e)  {
          logException($e);
          die('Error: database error.');
        }
    }    
    
    function GetNbActiveUsers() {
      try {
        $query = 'SELECT ConnectedUsers '
               . 'FROM ChatRoom '
               . 'WHERE (Hidden = ?) '
               . '  AND ((DateEnd = 0) OR (DateEnd > ?)) '
               . '  AND (DateLastNewMessage > ?)';          
        $req = $this->db->prepare($query);
        $time = $_SERVER['REQUEST_TIME'];
        $idleTime = $time - (DAYS_TO_DELETE_IDLE_CHATROOM * 24 * 60 * 60);          
        $req->execute(array('0', $time, $idleTime));

        $activeUsers = 0;

        while ($line = $req->fetch()) { 
          $users = json_decode($line['ConnectedUsers'], true);
          $activeUsers += count($users);
        } 

        return $activeUsers;
      } catch (Exception $e)  {
        logException($e);
        die('Error: database error.');
      }
    }    
    
}
