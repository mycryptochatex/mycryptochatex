<?php

	class ChatRoom {
		public $id;
    public $name;
		public $dateCreation;
		public $dateLastNewMessage;
		public $users = array();
		public $messages = array();
		public $dateEnd;
		public $noMoreThanOneVisitor;
		public $isRemovable;
		public $removePassword;
    public $hidden;
		public $userId;
	}
  
	class ChatMessage {
		public $message;
		public $hash;
		public $userId;
		public $date;
	}  
  
  class ChatRoomDataTable {
    public $name;
    public $userCnt;
    public $dateCrated;
    public $dateLastMessage;    
    public $dateEnded;
    public $link;
  }  
  
  class JqueryDataTable {
    public $draw;
    public $recordsTotal;
    public $recordsFiltered;
    public $data = array();
  }

  ?>