<?php
    require 'inc/constants.php';
    require 'inc/conf.php';
    require 'inc/init.php';
    require 'inc/functions.php';
    require 'inc/classes.php';
    require 'inc/dbmanager.php';

    $dbManager = new DbManager();

    if (isset($_POST[GENERATE_PASSWORD_TOKEN_NAME])) {
      $generatePsw = ($_POST[GENERATE_PASSWORD_TOKEN_NAME] === GENERATE_PASSWORD_TOKEN_VALUE);         
    } else {
      $generatePsw = false;
    }    
    
    if (isset($_GET['id'])) {
      $roomId = $_GET['id'];         
    } else {
      $roomId = -1;
    }
    
    $chatRoom = $dbManager->GetChatroom($roomId);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Private chat room - MyCryptoChatEx by salikovi.cz</title>
    
    <link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />       
    <meta name="viewport" content="width=device-width" />
    
  <?php if(!defined('DEV_MODE')): ?>    
    <link href="styles/rel/myCryptoChat.min.css" rel="stylesheet" />
    <link href="styles/rel/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="styles/rel/jquery-ui.min.css" rel="stylesheet" />
    <link href="styles/rel/jquery-ui.structure.min.css" rel="stylesheet" />
    <link href="styles/rel/jquery-ui.theme.min.css" rel="stylesheet" />        
    <script src="scripts/rel/modernizr-2.6.2.min.js"></script>    
  <?php else: ?>     
    <link href="styles/dev/myCryptoChat.css" rel="stylesheet" />
    <link href="styles/dev/jquery.dataTables.css" rel="stylesheet" />
    <link href="styles/dev/jquery-ui.css" rel="stylesheet" />
    <link href="styles/dev/jquery-ui.structure.css" rel="stylesheet" />
    <link href="styles/dev/jquery-ui.theme.css" rel="stylesheet" />        
    <script src="scripts/dev/modernizr-2.6.2.js"></script>
  <?php endif; ?>
    
</head>
<body>
    <header>
        <div class="content-wrapper">
            <div class="float-left">
                <p class="site-title"><a href="index.php">MyCryptoChatEx</a></p>
            </div>
            <div class="float-right">
                <section id="login">
                </section>
                <nav>
                    <ul id="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="stats.php">Stats</a></li>
                        <li><a href="about.php">About</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <div id="body">
        <section class="content-wrapper main-content clear-fix">
            <h2>MyCryptoChatEx</h2>
            <div class="mb20">Chat with friends without anyone spying on what you say!</div>

            <div id="roomName">
              <?php 
                $roomLink = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                echo $chatRoom->name . " ( <a href=\"$roomLink\">$roomLink</a> )";
              ?>              
            </div>
            
            <div id="roompassword">
              Password: <input type="password" id="inputRoomPassword" /> <input type="button" value="" id="glyphicon-eye-open" class="glyphicon" />
              <input type="button" value="Set room password" id="btnRoomPassword" onclick="setRoomPassword();" />              
            </div>
            
            <div id="chatroom"></div>
            <div id="divUsers"><span id="nbUsers">1</span> user(s) online</div>

            <div id="message">
              Name: <input type="text" id="userName" /><br />
              <textarea id="textMessage" onkeydown="if (event.keyCode == 13 && !event.shiftKey) { sendMessage(); }"></textarea><br />
              <input type="button" value="Send" id="sendMessage" onclick="sendMessage();" /><br /><br />
              <?php 
                if($chatRoom->isRemovable) {
              ?>
                <br />
                <div id="divButtonRemoveChatroom">
                  <input type="button" value="Remove the chat room" onclick="removeChatroom(<?php if($chatRoom->removePassword != '') { echo 'true'; } else { echo 'false'; } ?>);" />
                </div>
              <?php
                }
              ?>
            </div>
        </section>
    </div>
    <footer>
        <div class="content-wrapper">
            <div class="float-left">
                <p>&copy; 2016 - MyCryptoChatEx <?php echo MYCRYPTOCHAT_VERSION; ?> by salikovi.cz</p>
            </div>
        </div>
    </footer>

  <?php if(!defined('DEV_MODE')): ?>    
    <script src="scripts/rel/jquery.min.js"></script>
    <script src="scripts/rel/jquery.dataTables.min.js"></script>
    <script src="scripts/rel/jquery-ui.min.js"></script>   
  <?php else: ?>     
    <script src="scripts/dev/jquery.js"></script>
    <script src="scripts/dev/jquery.dataTables.js"></script>
    <script src="scripts/dev/jquery-ui.js"></script>
  <?php endif; ?>  

    <script type="text/javascript">
        var generatePsw = '<?php echo $generatePsw; ?>';
        var roomId = '<?php echo htmlspecialchars($roomId, ENT_QUOTES, 'UTF-8'); ?>';
        var dateLastGetMessages = '<?php echo microtime(true) - 24*60*60*365*3; ?>';
    </script>
    
    <?php
      switch (rand(1, 2)) {
        case 1:
          ?> <script type="text/javascript" src="scripts/wordlist/jiri_kulhanek.js"></script> <?php
          break;
        case 2:
          ?> <script type="text/javascript" src="scripts/wordlist/cimrman.js"></script> <?php
          break;
        case 3:
          ?> <script type="text/javascript" src="scripts/wordlist/poznamky.js"></script> <?php
          break;        
        default:
          ?> <script type="text/javascript" src="scripts/wordlist/jiri_kulhanek.js"></script> <?php          
      }    
    ?>    
    <script type="text/javascript" src="scripts/wordlist/passwordGenerator.js"></script>

  <?php if(!defined('DEV_MODE')): ?>       
    <script type="text/javascript" src="scripts/rel/zerobin.min.js"></script>
    <script type="text/javascript" src="scripts/rel/myCryptoChat.min.js"></script>
  <?php else: ?>         
    <script type="text/javascript" src="scripts/dev/base64.js"></script>
    <script type="text/javascript" src="scripts/dev/rawdeflate.js"></script>
    <script type="text/javascript" src="scripts/dev/rawinflate.js"></script>
    <script type="text/javascript" src="scripts/dev/vizhash.js"></script>
    <script type="text/javascript" src="scripts/dev/sjcl.js"></script>
    
    <script type="text/javascript" src="scripts/dev/zerobin.js"></script>
    <script type="text/javascript" src="scripts/dev/myCryptoChat.js"></script>
  <?php endif; ?>
    
</body>
</html>
