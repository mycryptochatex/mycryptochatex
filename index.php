<?php
  require 'inc/dbmanager.php';
  require 'inc/constants.php';
  require 'inc/functions.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Home - MyCryptoChatEx by salikovi.cz</title>    
    
    <link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />        
    <meta name="viewport" content="width=device-width" />
    
  <?php if(!defined('DEV_MODE')): ?>    
    <link href="styles/rel/myCryptoChat.min.css" rel="stylesheet" />
    <link href="styles/rel/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="styles/rel/jquery-ui.min.css" rel="stylesheet" />
    <link href="styles/rel/jquery-ui.structure.min.css" rel="stylesheet" />
    <link href="styles/rel/jquery-ui.theme.min.css" rel="stylesheet" />   
    <script src="scripts/rel/modernizr-2.6.2.min.js"></script>
  <?php else: ?>     
    <link href="styles/dev/myCryptoChat.css" rel="stylesheet" />
    <link href="styles/dev/jquery.dataTables.css" rel="stylesheet" />
    <link href="styles/dev/jquery-ui.css" rel="stylesheet" />
    <link href="styles/dev/jquery-ui.structure.css" rel="stylesheet" />
    <link href="styles/dev/jquery-ui.theme.css" rel="stylesheet" />   
    <script src="scripts/dev/modernizr-2.6.2.js"></script>
  <?php endif; ?>    
    
</head>
<body>
    <?php
    
    $sqliAvailable = true;
    $showContent = true;
    $configIncluded = false;    
    
    if(is_readable(ROOT_DIR . CONFIG_FILE_NAME)) {
        include ROOT_DIR . CONFIG_FILE_NAME;
        $configIncluded = true;
    } else {
        $showContent = false;
        ?>
        <h2>Error: missing inc/conf.php</h2>
        <p>
            MyCryptoChatEx can't read the configuration file.<br />
            Copy <strong>inc/conf.template.php</strong> into <strong>inc/conf.php</strong>, and don't forget to <strong>customize it</strong>.
        </p>
    <?php
    }
    if (!extension_loaded('PDO')) {
        $showContent = false;
        $sqliAvailable = false;
    ?>
    <h2>Error: PDO missing</h2>
    <p>
        The PDO module is missing.<br />
        Please add it and load it to make this website work.
    </p>
    <?php
    }    
    if (!extension_loaded('PDO_SQLITE')) {
        $showContent = false;
        $sqliAvailable = false;
    ?>
    <h2>Error: PDO SQLite missing</h2>
    <p>
        The PDO SQLite module is missing.<br />
        Please add it and load it to make this website work.
    </p>
    <?php
    }    
    
    if ($sqliAvailable) {
      
      //create clean database
      $dbManager = new DbManager();      
      if (!file_exists(ROOT_DIR . DB_FILE_NAME)) {
        $dbManager->RecreateDatabase();
      }     
      
      //db check
      if(!is_writable(ROOT_DIR . DB_FILE_NAME)) {
        $showContent = false;
      ?>
      <h2>Error: database access</h2>
      <p>
        MyCryptoChatEx can't edit the database file.<br />
        Please give all rights to the apache (or current) user on the 'chatrooms.sqlite' file.
      </p>
      <?php
      }      
      
    } //PDO extension is available
        
    if(!file_exists(ROOT_DIR . LOGS_FILE_NAME)) {
      touch(ROOT_DIR . LOGS_FILE_NAME);
    }    
    if(!is_writable(ROOT_DIR . LOGS_FILE_NAME)) {
        $showContent = false;
    ?>
    <h2>Error: logs file access</h2>
    <p>
        MyCryptoChatEx can't edit the logs file.<br />
        Please give all rights to the apache (or current) user on the 'logs.txt' file.
    </p>
    <?php
    }
    
    
    if (version_compare(phpversion(), '5.3.29', '<')) {
        $showContent = false;
    ?>
    <h2>Error: php version</h2>
    <p>
        The version of php is <?php echo phpversion(); ?> and this is too low.<br />
        You need at least PHP 5.4 to run this website.
    </p>
    <?php
    }    
    
    if($configIncluded === true && SEED == 'f-rjng24!1r5TRHHgnjrt') {
        $showContent = false;
    ?>
    <h2>Error: the seed was not modified</h2>
    <p>
        The seed that is used to do a better hashing for users is still 'f-rjng24!1r5TRHHgnjrt'<br />
        Please modify its value in 'inc/conf.php'.<br />
        You could may be use '<?php echo randomString(20); ?>', or another.
    </p>
    <?php
    }
    
    if($showContent) {
    ?>
    <noscript>
        This website needs JavaScript activated to work. 
              <style>
                  div {
                      display: none;
                  }
              </style>
    </noscript>
    <header>
        <div class="content-wrapper">
            <div class="float-left">
                <p class="site-title"><a href="index.php">MyCryptoChatEx</a></p>
            </div>
            <div class="float-right">
                <section id="login">
                </section>
                <nav>
                    <ul id="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="stats.php">Stats</a></li>
                        <li><a href="about.php">About</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <div id="body">
        <section class="content-wrapper main-content clear-fix">

          <h2>MyCryptoChatEx</h2>
          <div class="mb50">Chat with friends without anyone spying on what you say!</div>

          <div class="chatRoomList mb50">
            <table id="chatRoomTable" class="hover compact" cellspacing="0" width="100%">
                  <thead>
                      <tr>                      
                          <th>Chat room name</th>                          
                          <th>Users count</th>
                          <th>Created</th>
                          <th>Expiration</th>
                          <th>Last message</th>
                          <th>Link</th>
                      </tr>
                  </thead>
                  <tfoot>
                      <tr>
                          <th>Chat room name</th>                          
                          <th>Users count</th>
                          <th>Created</th>
                          <th>Expiration</th>
                          <th>Last message</th>
                          <th>Link</th>
                      </tr>
                  </tfoot>
              </table>            
          </div>
            
          <form method="POST" action="newroom.php">
            <label for="roomName">Name of the chat room:</label>
            <input type="text" name="roomName" value="<?php echo $dbManager->GetNextRoomName(); ?>" />
            <br />
            
            <label for="nbMinutesToLive">Lifetime of the chat room:</label>
            <select id="nbMinutesToLive" name="nbMinutesToLive">
              <?php foreach ($allowedTimes as $minutes => $label) { ?>
                  <option value="<?php echo $minutes; ?>"><?php echo $label; ?></option>
              <?php } ?>
            </select><br />
            <br />
				
            <input type="checkbox" name="isRemovable" id="isRemovable" value="true" onchange="if(this.checked) { $('#divRemovePassword').show(); } else { $('#divRemovePassword').hide(); }" />
            <label for="isRemovable" class="labelIndex">Is removable</label>
            <br />
            
            <div id="divRemovePassword">
              <br />
              <label for="removePassword">Password to remove:</label>
              <input type="password" name="removePassword" value="" />
            </div>
            <br />
				
            <input type="checkbox" name="selfDestroys" id="selfDestroys" value="true" />
            <label for="selfDestroys" class="labelIndex">Self-destroys if more than one visitor</label>            
            <br />				
            <br />
            
            <input type="checkbox" name="hiddenRoom" id="hiddenChatRoom" value="true" />
            <label for="hiddenChatRoom" class="labelIndex">Chat room will not be visible in rooms list</label>            
            <br />				
            <br />            
            
            <input type="submit" value="Create a new chat room" />
          </form>
            
        </section>
    </div>
    <footer>
        <div class="content-wrapper">
            <div class="float-left">
                <p>&copy; 2016 - MyCryptoChatEx <?php echo MYCRYPTOCHAT_VERSION; ?> by salikovi.cz</p>
            </div>
        </div>
    </footer>
    
  <?php if(!defined('DEV_MODE')): ?>    
    <script src="scripts/rel/jquery.min.js"></script>
    <script src="scripts/rel/jquery.dataTables.min.js"></script>
    <script src="scripts/rel/jquery-ui.min.js"></script>
  <?php else: ?>     
    <script src="scripts/dev/jquery.js"></script>
    <script src="scripts/dev/jquery.dataTables.js"></script>
    <script src="scripts/dev/jquery-ui.js"></script>
  <?php endif; ?>         
    
    <script type="text/javascript">
      $('#chatRoomTable').DataTable( {
        //"processing": true,
        //"serverSide": true,
        "ajax": "roomslist.php"
      } );
    </script>    
    
    <?php } //if-showcontent ?>
</body>
</html>
