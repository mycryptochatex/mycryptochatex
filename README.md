MyCryptoChatEx
==============

MyCryptoChatEx is modification of the MyCryptoChat project from HowTomy (https://github.com/HowTommy).
It is a simple PHP encrypted chat rooms manager.
Everything is encrypted on the client side, so noone can spy on what you say.