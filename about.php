<?php
  require 'inc/conf.php';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>About - MyCryptoChatEx by SaLIk</title>
        
        <link rel="icon" href="/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />               
        
        <meta name="viewport" content="width=device-width" />
        
      <?php if(!defined('DEV_MODE')): ?>    
        <link href="styles/rel/myCryptoChat.min.css" rel="stylesheet"/>
        <script src="scripts/rel/modernizr-2.6.2.min.js"></script>
      <?php else: ?>     
        <link href="styles/dev/myCryptoChat.css" rel="stylesheet"/>
        <script src="scripts/dev/modernizr-2.6.2.js"></script>
      <?php endif; ?>                  

    </head>
    <body>
        <header>
            <div class="content-wrapper">
                <div class="float-left">
                    <p class="site-title"><a href="index.php">MyCryptoChatEx</a></p>
                </div>
                <div class="float-right">
                    <section id="login">
                    </section>
                    <nav>
                        <ul id="menu">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="stats.php">Stats</a></li>
                            <li><a href="about.php">About</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <div id="body">
            
            <section class="content-wrapper main-content clear-fix">
 <h2>About</h2>

<p>
    MyCryptoChatEx is a simple PHP encrypted chat rooms manager. Everything is encrypted on the client side, so noone can spy on what you say.<br />
    <br />
    
    <a href="https://gitlab.com/groups/mycryptochatex">More informations here</a>
    <br />
    <br />   
    
    Author of original <a href="https://github.com/HowTommy/mycryptochat">MyCryptoChat</a>
    is Tommy of <a href="http://blog.howtommy.net">HowTommy.net</a><br />
    <a href="http://blog.howtommy.net/?d=2010/01/01/01/01/01-me-contacter">You can contact him</a>    
    <br />
    <br />        
    
    Author of <a href="https://gitlab.com/mycryptochatex/mycryptochatex">MyCryptoChatEx</a> modification
    is SaLIk of <a href="http://www.salikovi.cz">salikovi.cz</a><br />
    <a href="mailto:email@salikovi.cz">You can contact me</a>
    
</p>
            </section>
        </div>
        <footer>
            <div class="content-wrapper">
                <div class="float-left">
                    <p>&copy; 2016 - MyCryptoChatEx <?php require 'inc/constants.php'; echo MYCRYPTOCHAT_VERSION; ?> by salikovi.cz</p>
                </div>
            </div>
        </footer>

      <?php if(!defined('DEV_MODE')): ?>    
        <script src="scripts/rel/jquery.min.js"></script>
      <?php else: ?>     
        <script src="scripts/dev/jquery.js"></script>
      <?php endif; ?>             
        
    </body>
</html>
