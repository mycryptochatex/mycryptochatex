<?php

require 'inc/conf.php';
require 'inc/constants.php';
require 'inc/init.php';
require 'inc/functions.php';
require 'inc/classes.php';
require 'inc/dbmanager.php';

if (isset($_GET['draw'])) {
  $draw = $_GET['draw'];
} else {
  $draw = 1;
}

$dbManager = new DbManager();
$rooms = $dbManager->GetRoomList();

$users = array();
$dataTable = new JqueryDataTable();
foreach ($rooms as $room) {  
    /*
		public $id;
    public $name;
		public $dateCreation;
		public $dateLastNewMessage;
		public $users = array();
		public $messages = array();
		public $dateEnd;
		public $noMoreThanOneVisitor;
		public $isRemovable;
		public $removePassword;
    public $hidden
		public $userId;
     */  
  
  $users = json_decode($room->users, true);    
  $roomLink = "<a href=\"chatroom.php?id=$room->id\">Open chat room</a>";
  
  $dataTable->data[] = array(     
    $room->name,
    count($users),
    date('d.m.Y H:i:s', $room->dateCreation),
    $room->dateEnd != 0 ? date('d.m.Y H:i:s', $room->dateEnd) : date('d.m.Y H:i:s', $room->dateLastNewMessage + (DAYS_TO_DELETE_IDLE_CHATROOM * 24 * 60 * 60)),  //'&infin;'
    date('d.m.Y H:i:s', $room->dateLastNewMessage),    
    $roomLink
  );
}
$dataTable->draw = $draw;
$dataTable->recordsTotal = count($dataTable->data);
$dataTable->recordsFiltered = count($dataTable->data);

echo json_encode($dataTable);

?>